<?php
/**
 * Plugin Name:     Bento Blocks
 * Plugin URI:      back2front.ca
 * Description:     Creates content blocks used for theme development
 * Author:          Curtis Peters
 * Author URI:      curtispeters.com
 * Text Domain:     bento_blocks
 * Version:         0.1.1
 *
 * @package         Bento_blocks
 */

// Your code starts here.
define( 'BENTO_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'BENTO_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'BENTO_THEME_PATH', get_stylesheet_directory() );
define( 'BENTO_THEME_TEMPLATE_PATH', BENTO_THEME_PATH . '/loop-templates/' );
define( 'BENTO_PARENT_PATH', get_template_directory() );


/* notify user to download required plugins */
require BENTO_PLUGIN_PATH . 'includes/class-tgm-plugin-activation.php';
/* Include packaged plugins */
require BENTO_PLUGIN_PATH . 'enqueue_plugins.php';



/* Include functions */
require BENTO_PLUGIN_PATH . 'functions/register_acf_blocks.php';
require BENTO_PLUGIN_PATH . 'functions/filters.php';
require BENTO_PLUGIN_PATH . 'functions/options_page.php';
require BENTO_PLUGIN_PATH . 'functions/functions.php';
require BENTO_PLUGIN_PATH . 'functions/tinymce_extend_styles.php';
// acf fields
require BENTO_PLUGIN_PATH . 'functions/bento_fields.php';



/**
 * Enqueues debug info to the footer of the page
 * if the user is logged in and is an administrator
 * Still a good idea to turn this off for production
 * 
 */
function enqueue_debug_info(){
	if(is_user_logged_in() && current_user_can( 'administrator' )){
		require BENTO_PLUGIN_PATH . 'debug.php';
	}
}
add_action( 'init', 'enqueue_debug_info', $priority = 10, $accepted_args = 1 );


/**
 * Functions to run when the theme first activates
 * 
 */
function bento_plugin_activation_initial_tasks() {


    /**
     * Adds a sass folder for default styles in theme directory
     * if one doesn't already exist
     */
    if(!file_exists(BENTO_THEME_PATH . '/sass/bento_customization.scss')){
        //The name of the directory that we need to create.
        $directoryName = BENTO_THEME_PATH . '/sass';
        $css_dir = BENTO_THEME_PATH . '/css';

        //Check if the directory already exists.
        if(!is_dir($directoryName)){
            //Directory does not exist, so lets create it.
            mkdir($directoryName, 0755);
        }
        //Check if the directory already exists.
        if(!is_dir($css_dir)){
            //Directory does not exist, so lets create it.
            mkdir($css_dir, 0755);
        }
    }


    if(is_dir(BENTO_THEME_PATH . '/sass/')){
        $content = "";
        $css_file = BENTO_THEME_PATH . '/sass/bento_customization.scss';
        $bootstrap_path = BENTO_PARENT_PATH . '/src/sass/bootstrap4/bootstrap.scss';
        
        $content .= '@import "'. $bootstrap_path . '";';
        //write the file
        file_put_contents($css_file, $content);
    }

    /**
     * Adds a css folder for complied styles in theme directory
     * if one doesn't already exist
     */
    if(!file_exists(BENTO_THEME_PATH . '/css/bento_customization.scss')){
        //The name of the directory that we need to create. 
        $css_dir = BENTO_THEME_PATH . '/css';
        
        //Check if the directory already exists.
        if(!is_dir($css_dir)){
            //Directory does not exist, so lets create it.
            mkdir($css_dir, 0755); }

        
    }
    
}
register_activation_hook( __FILE__, 'bento_plugin_activation_initial_tasks' );


/**
 * Bento Overrides
 *
 * Function to run after the theme is setup and thus override unwanting things done via the theme
 */
add_action( 'after_setup_theme', 'bento_theme_setup' );

if ( ! function_exists( 'bento_theme_setup' ) ) {
    /**
     
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function bento_theme_setup() {
        /**
         * Remove the "read more" excerpt string link to the post added by understrap
         *
         * @param string $more "Read more" excerpt string.
         * @return string (Maybe) modified "read more" excerpt string.
         */
        if(function_exists('understrap_all_excerpts_get_more_link')){
            remove_filter( 'wp_trim_excerpt', 'understrap_all_excerpts_get_more_link' );
        }

    }

}

