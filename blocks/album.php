<?php
/**
 * Album Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'album-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'album';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$key = $block['id'];

$classes = esc_attr($className);

?>


<div class="<?php echo $classes ?>">
    <div class="container">

      <div class="row">
        <?php if( have_rows('album') ): ?>
          <?php while( have_rows('album') ): the_row(); 


          // Load values and assing defaults. 
          
          $title = get_sub_field('title');
          $content = get_sub_field('content');
          $button_text = get_sub_field('button_text');
          $category = get_sub_field('category');
          $image_obj = get_sub_field('image');

          $image = bento_return_image_dimensions($image_obj, 'large');


          ?>
          <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
              <picture>
                <source srcset="<?php echo $image['url'] ?>" type="image/svg+xml">
                  <img src="<?php echo $image['url'] ?>" class="img-fluid" alt="<?php echo $image_obj['alt'] ?>">
              </picture>
              <div class="card-body">
                <p class="card-text"><?php echo $content ?></p>
                <div class="d-flex justify-content-between align-items-center">
                  <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-outline-secondary"><?php echo $button_text ?></button>
                    
                  </div>
                  <small class="text-muted"><?php echo $category ?></small>
                </div>
              </div>
            </div>
          </div>
        

        <?php endwhile; ?>
      <?php endif; ?>
      </div>
    </div>
  </div>

