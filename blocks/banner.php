<?php
/**
 * Banner Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'banner-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'banner';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assing defaults.
$headline = get_field('headline') ?: 'Headline';
$tagline = get_field('tagline') ?: 'Tagline here';


$buttons = bento_get_cta_button_info();



$background = bento_block_background();
$headings = bento_get_section_heading();

$classes = esc_attr($className);


?>

<div class="<?php echo $classes ?> container-fluid" <?php echo $background ?>>
  <div class="row">
    <div class="col-md-10 p-lg-10 mx-auto">
      <h1>
        <span><?php echo $headings['heading'] ?></span>
        <?php if( array_key_exists('sub_heading', $headings) ): ?>
          <small><?php echo $headings['sub_heading'] ?></small>
        <?php endif; ?>
      </h1>
      <?php if( array_key_exists('intro', $headings) ): ?>
        <p><?php echo $headings['intro'] ?></p>
      <?php endif; ?>
      <?php if($buttons): ?>
        <?php foreach ($buttons as $button): 
          $btnclass = "";
          $target = "";
          if($button['open_externally']){
              $target = "target='_blank'";
              $btnclass .= " external_link";
          }
          ?>
              
          <a class="btn btn-<?php echo $button['style']; ?> <?php echo $btnclass; ?>" href="<?php echo $button['url'] ?>" <?php echo $target; ?>><?php echo $button['text'] ?></a>
        <?php endforeach; ?>
      <?php endif; ?>

    </div>
    <div class="product-device shadow-sm d-none d-md-block"></div>
    <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
  </div>
</div>