<?php

/**
 * Contact Us Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'contact-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'contact';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assing defaults.

$description = get_field('description');
$form = get_field('form');
$background = bento_block_background();

$description_column_classes = get_field( 'description_column_classes' ) ?: 'col';
$form_column_classes = get_field( 'form_column_classes' ) ?: 'col';
$classes = esc_attr($className);
?>


<div class="<?php echo $classes; ?>" <?php echo $background ?>>
  <div class="container">
    <div class="row">
      <div class="<?php echo $description_column_classes; ?>">
        <?php echo $description ?>
      </div>
      <div class="<?php echo $form_column_classes ?>">
        <?php echo do_shortcode( $form, $ignore_html = false ) ?>
      </div>
    </div>
  </div>
</div>