<?php

/**
 * Featurette Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'featurette-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'featurette';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assing defaults.
$heading = get_field('heading') ?: 'Your heading here...';
$content = get_field('content') ?: 'Description';
$image = get_field('image');
$image_size = 'large';
$the_image = $image['sizes'][$image_size];
$image_width = $image['sizes'][$image_size . '-width'];
$image_height = $image['sizes'][$image_size . '-height'];

$classes = esc_attr($className);

$image_column_classes = get_field( 'image_column_classes' ) ?: 'col';
$content_column_classes = get_field( 'content_column_classes' ) ?: 'col';

$headings = bento_get_section_heading();
$background = bento_block_background();

?>

<div class="<?php echo $classes ?>" <?php echo $background ?>>
  <div class="container">
    <?php if( $headings ): ?>
    <div class="row">
      <div class="col">
        <h1><?php echo $headings['heading'] ?></h1>
        <p><?php echo $headings['intro'] ?></p>
      </div>
    </div>
    <?php endif; ?>
    <div class="row">
      <div class="<?php echo $image_column_classes ?>">
        <img src="<?php echo $the_image ?>" class="img-fluid">
      </div>
      <div class="<?php echo $content_column_classes ?>">
        <?php echo $content ?>
      </div>
    </div>
  </div>
</div>