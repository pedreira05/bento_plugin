<?php
/**
 * Marketing Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'marketing-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'marketing';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$background = bento_block_background('marketing');

$classes = esc_attr($className);
?>

<div class="<?php echo $classes ?>" <?php echo $background ?>>
  <div class="container">
    <?php if( have_rows('section_intro') ): ?>
    <div class="row">
      <div class="col">
           <?php while( have_rows('section_intro') ): the_row(); 
              $heading = get_sub_field('heading');
              $sub_heading = get_sub_field('sub-heading');
              $content = get_sub_field('content');

            ?>
            <h2><?php echo $heading  ?></h2>
            <?php echo $sub_heading; ?>
           <?php endwhile; ?>
      </div>
    </div>
    <?php endif; ?>
      <div class="row">
        <?php if( have_rows('marketing_blocks') ): ?>
            

            <?php while( have_rows('marketing_blocks') ): the_row(); 

                // Load values and assing defaults. 
                $heading = get_sub_field('heading');
                $content = get_sub_field('content');
                $image = get_sub_field('image');
                $button_label = get_sub_field('button_label');


                ?>
              <div class="col marketing-card-col">
                <div class="marketing-card">
                  <img src="<?php echo $image['sizes']['large']; ?>" class="img-fluid" alt="<?php echo $image['alt'] ?>">
                <h2><?php echo $heading ?></h2>
                <?php echo $content ?>
                <a class="btn btn-secondary" href="#" role="button"><?php echo $button_label ?></a></p>
                </div><!-- marketing-card -->
              </div><!-- /.col-lg-4 -->
              

            <?php endwhile; ?>
          <?php endif; ?>
        </div><!-- /.row -->
    </div>
  </div>