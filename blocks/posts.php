<?php
/**
 * Marketing Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'marketing-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'marketing';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$background = bento_block_background('marketing');

$classes = esc_attr($className);
?>

<div class="<?php echo $classes ?>" <?php echo $background ?>>
  <div class="container">
    <?php if( have_rows('section_intro') ): ?>
    <div class="row">
      <div class="col">
           <?php while( have_rows('section_intro') ): the_row(); 
              $heading = get_sub_field('heading');
              $sub_heading = get_sub_field('sub-heading');
              $content = get_sub_field('content');

            ?>
            <h2><?php echo $heading  ?></h2>
            <?php echo $sub_heading; ?>
           <?php endwhile; ?>
      </div>
    </div>
    <?php endif; ?>


    
      <div class="row">
            <?php 

            // prepare posts if category is selected
            if( get_field( 'bento_post_category' ) ): 
            
                $bento_post_category = get_field( 'bento_post_category' );
                $number_of_posts = get_field( 'number_of_posts_to_show' ); 
                $args = array(
                    'post_type' => $bento_post_category,
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'posts_per_page' => $number_of_posts
                     );
                $query = new WP_Query( $args );
                $the_bento_posts = $query->posts;
                ?>
                <?php if( $the_bento_posts ): ?>
                          <?php foreach( $the_bento_posts as $bpost ): ?>
                            <?php
                              if(file_exists( BENTO_THEME_TEMPLATE_PATH . 'content-bento-posts.php' )){
                                 include( BENTO_THEME_TEMPLATE_PATH . 'content-bento-posts.php' );
                              } else {
                                include(BENTO_PLUGIN_PATH . 'template_parts/posts.php'); 
                              }
                            ?>
                          <?php endforeach; ?>
              <?php endif;   ?>

                    
        <?php endif; ?>


        <?php // run this if the user selected to put specific posts ?>
        <?php if( have_rows('bento_posts_repeater') ): ?>
            

            <?php while( have_rows('bento_posts_repeater') ): the_row(); 

                // Load values and assing defaults. 
                $bento_post = get_sub_field('bento_posts'); ?>
                <?php if( $bento_post ): ?>
                  <?php foreach( $bento_post as $bpost ): ?>
                    <?php
                      if(file_exists( BENTO_THEME_TEMPLATE_PATH . 'content-bento-posts.php' )){
                         include( BENTO_THEME_TEMPLATE_PATH . 'content-bento-posts.php' );
                      } else {
                        include(BENTO_PLUGIN_PATH . 'template_parts/posts.php'); 
                      }
                    ?>
                  <?php endforeach; ?>
                <?php endif; ?>
              

            <?php endwhile; ?>
          <?php endif; ?>

          
        </div><!-- /.row -->
    </div>
  </div>