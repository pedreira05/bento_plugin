<?php
/**
 * Pricing Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'pricing-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'pricing';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assing defaults.
$headline = get_field('headline') ?: 'Headline';
$tagline = get_field('tagline') ?: 'Tagline here';

$background = bento_block_background();




$headings = bento_get_section_heading();
$classes = esc_attr($className);



?>

<div class="container <?php echo $classes ?>" <?php echo $background ?>>
  <?php if( have_rows('section_intro') ): ?>
  <div class="row">
    <div class="col">
         <?php while( have_rows('section_intro') ): the_row(); 
            $heading = get_sub_field('heading');
            $sub_heading = get_sub_field('sub_heading');
            $content = get_sub_field('intro');

          ?>
          <h2><?php echo $heading  ?></h2>
          <?php echo $sub_heading; ?>
          <p><?php echo $content ?></p>
         <?php endwhile; ?>
    </div>
  </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-12">
      
    
      <div class="card-deck mb-3 text-center">
        <?php if( have_rows('pricing_blocks') ): ?>
            <?php while( have_rows('pricing_blocks') ): the_row(); 

                // Load values and assing defaults. 
                $product = get_sub_field('product');
                $price = get_sub_field('price');
                $quantity = get_sub_field('quantity');
                $description = get_sub_field('description');
                $buttons = bento_get_cta_button_info();
                ?>
              <div class="card mb-4 shadow-sm">
                <div class="card-header">
                  <h4 class="my-0 font-weight-normal"><?php echo $product ?></h4>
                </div>
                <div class="card-body">
                  <h1 class="card-title pricing-card-title"><?php echo $price ?> <small class="text-muted"><?php echo $quantity ?></small></h1>
                  <?php echo $description ?>


                  <?php if($buttons): ?>
                    <?php foreach ($buttons as $button): ?>
                      
                      <?php if($button['text']): ?>  
                      <a class="btn btn-<?php echo $button['style']; ?>" href="<?php echo $button['url'] ?>"><?php echo $button['text'] ?></a>
                    <?php endif; ?>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </div>
              </div>
              

            <?php endwhile; ?>
          <?php endif; ?>
        </div><!-- /.card-deck -->
  
    </div>
  </div>
</div>