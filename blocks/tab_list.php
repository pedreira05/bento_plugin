<?php
/**
 * Tab List Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tab_list-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'tab_list';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$key = $block['id'];

$classes = esc_attr($className);
?>

<div class="container">
    
  <div class="row <?php echo $classes ?>">
    <?php if( have_rows('tab_list') ): ?>
        <?php 
          $count=0;
        ?>

        <div class="col-4">
          <div class="list-group" id="<?php echo $id; ?>" role="tablist">
            <?php while( have_rows('tab_list') ): the_row(); 


            // Load values and assing defaults. 
            $active_class = ($count==0)? 'active' : '';
            $title = get_sub_field('title');
            $obj_title = get_sub_field_object('title');
            $slug = $obj_title['name'] . '_' . $key;
            $content = get_sub_field('content');

            ?>
            <a class="list-group-item list-group-item-action <?php echo $active_class ?>" id="list-<?php echo $slug ?>-list" data-toggle="list" href="#list-<?php echo $slug ?>" role="tab" aria-controls="<?php echo $slug ?>"><?php echo $title ?></a>
            <?php $count++; ?>
            <?php endwhile ?>
          </div>
        </div>
        <div class="col-8">
          <div class="tab-content" id="nav-tabContent">
            <?php $count=0; ?>
            <?php while( have_rows('tab_list') ): the_row(); 


            // Load values and assing defaults. 
              $active_class = ($count==0)? 'active' : ''; 
            $title = get_sub_field('title');
            $obj_title = get_sub_field_object('title');
            $slug = $obj_title['name'] . '_' . $key;
            $content = get_sub_field('content');

            ?>
              <div class="tab-pane fade show <?php echo $active_class ?>" id="list-<?php echo $slug ?>" role="tabpanel" aria-labelledby="list-<?php echo $slug ?>-list"><?php echo $content ?></div>
              <?php $count++; ?>
            <?php endwhile; ?>
            
          </div>
        </div>
    <?php endif; ?>
  </div>

</div>
