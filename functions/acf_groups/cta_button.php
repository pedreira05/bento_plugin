<?php 
acf_add_local_field_group(array(
	'key' => 'group_5c1268cd73983',
	'title' => 'Button',
	'fields' => array(
		array(
			'key' => 'field_5c1268d2c0b67',
			'label' => 'Button Label',
			'name' => 'text',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => 30,
		),
		array(
			'key' => 'field_5c1268e0c0b68',
			'label' => 'Button Style',
			'name' => 'style',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'primary' => 'Primary',
				'secondary' => 'Secondary',
				'outline-primary' => 'Outline Primary',
				'outline-secondary' => 'Outline Secondary',
			),
			'default_value' => array(
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'ajax' => 0,
			'placeholder' => '',
		),
		array(
			'key' => 'field_5c1268f8c0b69',
			'label' => 'Page',
			'name' => 'page',
			'type' => 'page_link',
			'instructions' => 'Select page.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array(
				0 => 'page',
			),
			'taxonomy' => '',
			'allow_null' => 0,
			'allow_archives' => 1,
			'multiple' => 0,
		),
		array(
			'key' => 'field_ia0ujwxmyd6q3',
			'label' => 'Custom URL',
			'name' => 'custom_url',
			'type' => 'text',
			'instructions' => 'Or set a custom URL',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => 100,
		),
		array(
			'key' => 'field_1k32os23ms023',
			'label' => 'Open in new window',
			'name' => 'open_externally',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => 30,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => false,
	'description' => '',
));
 ?>