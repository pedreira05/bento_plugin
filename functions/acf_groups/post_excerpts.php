<?php 

acf_add_local_field_group(array(
	'key' => 'group_5dbb1cae47ef7',
	'title' => 'Posts Group',
	'fields' => array(
		array(
			'key' => 'field_5dbb29f70e188',
			'label' => 'Content',
			'name' => '',
			'type' => 'accordion',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'open' => 0,
			'multi_expand' => 0,
			'endpoint' => 0,
		),
		array(
			'key' => 'field_5dc342b612b54',
			'label' => 'Type',
			'name' => 'type',
			'type' => 'select',
			'instructions' => 'Select Category to pull latest posts from that category. Select Post to choose individual posts',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'Category' => 'Category',
				'Post' => 'Post',
			),
			'default_value' => array(
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'ajax' => 0,
			'placeholder' => '',
		),
		array(
			'key' => 'field_5dbb1cbce8b79',
			'label' => 'Posts',
			'name' => 'bento_posts_repeater',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5dc342b612b54',
						'operator' => '==',
						'value' => 'Post',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => 0,
			'max' => 0,
			'layout' => 'row',
			'button_label' => '',
			'sub_fields' => array(
				array(
					'key' => 'field_5dbb1cd0e8b7a',
					'label' => 'Post',
					'name' => 'bento_posts',
					'type' => 'relationship',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'post_type' => '',
					'taxonomy' => '',
					'filters' => array(
						0 => 'search',
						1 => 'post_type',
						2 => 'taxonomy',
					),
					'elements' => '',
					'min' => '',
					'max' => '',
					'return_format' => 'object',
				),
			),
		),
		array(
			'key' => 'field_5dc3433f42fc7',
			'label' => 'Category',
			'name' => 'bento_post_category',
			'type' => 'post_type_selector',
			'instructions' => 'Choose the type of posts you would like to select from',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5dc342b612b54',
						'operator' => '==',
						'value' => 'Category',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'select_type' => 2,
		),
		array(
			'key' => 'field_5dc36be8f98d0',
			'label' => 'Number of Posts',
			'name' => 'number_of_posts_to_show',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5dc342b612b54',
						'operator' => '==',
						'value' => 'Category',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/posts',
			),
		),
	),
	'menu_order' => 40,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

 ?>