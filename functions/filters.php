<?php 


/**
 * Takes a given image size and image object from ACF and returns the width, height and url in an array
 * @param  obj $img_obj image object as set by ACF field
 * @param  string $size    The size of the image, thumbnail, medium, custom
 * @return array          array with the url widh and height of the image
 */
function bento_return_image_dimensions($image, $image_size){
	$the_image = $image['sizes'][$image_size];
	$image_width = $image['sizes'][$image_size . '-width'];
	$image_height = $image['sizes'][$image_size . '-height'];

	$image = array(
		'url'			=>			$the_image,
		'width'			=>			$image_width,
		'height'		=>			$image_height
	);

	return $image;
}

/**
 * Returns the inline style definition for the background image
 * or background color for the specified block
 *
 * 
 * @param  string $block_name name of the block. Get's name from the clone 'Name' value on ACF
 * @return string             css definition for inline style
 */
function bento_block_background(){
	$background_img = get_field('background_image');
	$background_color = get_field('background_color');

	$background = "style='background-color: $background_color;'";
	$bg_img = $background_img['sizes']['large'];
	if(!empty($bg_img)){
	  $background = "style='background: url($bg_img) no-repeat; background-size: cover; background-color: $background_color'";
	} 

	return $background;
}

/**
 * Return all the attributes of a cta button that
 * was set in the ACF cta button field
 * @return array array where the key is attribute name and value is the value
 */
function bento_get_cta_button_info(){
  /*
  * If this is a repeater field, fill values from subfield
   */
  $buttons = array(); 
  if ( have_rows( 'cta' ) ) {
    while ( have_rows( 'cta' ) ) : the_row(); 
      $the_button = array();
      $the_button['text'] = get_sub_field( 'text' ); 
      $the_button['style'] = get_sub_field( 'style' ); 
      $the_button['page'] = get_sub_field( 'page' ); 
      $the_button['section'] = get_sub_field( 'section' ); 
      $the_button['open_externally'] = get_sub_field( 'open_externally' ); 
      
      $custom_url = get_sub_field('custom_url');
      if(!empty( $custom_url )){
        $button_url = $custom_url;
      } else {
        $button_url = get_sub_field('page');
      }
      $the_button['url'] = $button_url;
      array_push($buttons, $the_button);
    endwhile; 
  } else {
    /**
     * Fill info of button from top-level field if not a repeater
     * 
     */
    $the_button = array();
    $the_button['text'] = get_sub_field( 'text' ); 
    $the_button['style'] = get_sub_field( 'style' ); 
    $the_button['page'] = get_sub_field( 'page' ); 
    $the_button['section'] = get_sub_field( 'section' ); 
    
    $custom_url = get_sub_field('custom_url');
    if(!empty( $custom_url )){
      $button_url = $custom_url;
    } else {
      $button_url = get_sub_field('page');
    }
    $the_button['url'] = $button_url;
    array_push($buttons, $the_button);
  }
  return $buttons;
}

/**
 * Return all values for the section intro. All values that are 
 * set under the 'section_intro' ACF block for the WP block that
 * requests it
 * @return array key is attribute name and value is value
 */
function bento_get_section_heading(){
  $values = null;
  if(have_rows('section_intro')){
    $values = array();
    while( have_rows('section_intro') ): the_row();
      $heading = get_sub_field( 'heading' );
      $sub_heading = get_sub_field( 'sub_heading' );
      $intro = get_sub_field( 'intro' );

      if ($heading) $values['heading'] = $heading;
      if ($sub_heading) $values['sub_heading'] = $sub_heading;
      if ($intro) $values['intro'] = $intro;



    endwhile; 
  }
  return $values;
}


/**
 * Returns a custom copyright declaration for the site
 * @return text site copyright info
 */
function bento_copyright_footer(){
  $copyright = get_field( 'copyright_notice', 'option' );
  echo $copyright;
}

