<?php 


/**
 * Take Variables set in the Options page and apply them to the
 * variables Scss file in the theme directory
 * @return file the _teme_variables.scss file
 */
function bento_save_global_css_file(){
	$screen = get_current_screen();
	if (strpos($screen->id, "acf-options-global-css") == true) {
		$theme_root = get_stylesheet_directory(); 
		$content = "";
		$css_file = fopen($theme_root . "/sass/theme/_theme_variables.scss", "w");

		// colour groupings in the ACF fields
		// Each group corresponds to a 'group' field in ACF
		// that is served on the optinos page
		$colour_groups = array(
			'theme_colors',
			'global-colors',
			'link_styles',
			'spacing',
			'headings'
		);

		$append_unit = array(
			'spacing'		=>		'rem',
			'headings'		=>		' * $font-size-base',
		);

		foreach ($colour_groups as $row_name) {
			//Get background colours
			if( have_rows($row_name, 'option') ):
				while( have_rows($row_name, 'option') ): the_row();

					// determine if a unit should be appended to the end of the value
					$unit = null;
					if(array_key_exists($row_name, $append_unit)){
						$unit = $append_unit[$row_name];
					}

					$global_colors_obj = get_field_object($row_name, 'option');
					$colors = $global_colors_obj['value'];
					foreach ($colors as $selector => $value) {
						if(!empty($value)){
							$content .= "$$selector : $value$unit;";
						}
					}
				endwhile;
			endif;
		}

		//write the file
		fwrite($css_file, $content);
		fclose($css_file);
	}

}
add_action('acf/save_post', 'bento_save_global_css_file', 20);




function bento_save_custom_scss(){
	$screen = get_current_screen();
	if (strpos($screen->id, "acf-options-global-css") == true) {
		$theme_root = get_stylesheet_directory(); 
		$content = "";
		$css_file = fopen($theme_root . "/sass/theme/_theme.scss", "w");

		// prepare the bento_core style scss to be copied to the theme directory
		$core_scss = BENTO_PLUGIN_PATH . "css/bento_core.scss";
		// copy core styles scss over to the theme directory so that it can be
		// imported into the _theme.scss file
		copy(BENTO_PLUGIN_PATH . "css/bento_core.scss", $theme_root . "/sass/theme/_bento_core.scss");

		$content .= "@import 'bento_core';";
		$content .= get_field( 'custom_css','option' );

		//write the file
		fwrite($css_file, $content);
		fclose($css_file);
	}

}
add_action('acf/save_post', 'bento_save_custom_scss', 20);




/**
 * Add custom generated CSS to theme
 *
 */
function bento_add_bento_styles(){
	if(file_exists(get_stylesheet_directory() . '/global_bento.css')){
		
		wp_enqueue_style( 'bento_customizations', get_stylesheet_directory_uri() . '/css/bento_customization.css', array(), '0.1', 'all' );
		// wp_enqueue_style( 'global_bento_styles', get_stylesheet_directory_uri() . '/global_bento.css', array('understrap-styles','understrap-master-style','bento-style'), '0.1', 'all' );
	}
}
add_action( 'wp_enqueue_scripts', 'bento_add_bento_styles', $priority = 10 );
add_action( 'admin_enqueue_scripts', 'bento_add_bento_styles', $priority = 10 );





/**
 * Replace the default understrap theme copyright with that
 * of Bento's copyright if the undestrap footer is detected
 * 
 */
function bento_replace_understrap_copyright(){
	if(function_exists('understrap_site_info')){
		remove_action( 'understrap_site_info', 'understrap_add_site_info' );
		add_action( 'understrap_site_info', 'bento_copyright_footer' );
	}
}
add_action( 'wp_head', 'bento_replace_understrap_copyright' );

//Add page name to  body tag.  Page Slug Body Class
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

//enqueues our external font awesome stylesheet
function enqueue_our_required_stylesheets(){
	// font-awesome
	wp_enqueue_style('font-awesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css'); 
	wp_enqueue_script('bento-core', BENTO_PLUGIN_URL . 'js/bento_core.js', 'jquery',null, true);

}
add_action('wp_enqueue_scripts','enqueue_our_required_stylesheets');





 ?>