<?php 

if( function_exists('acf_add_options_page') ) {
	

	acf_add_options_page(array(
		'page_title' 	=> 'Bento General Settings',
		'menu_title'	=> 'Bento Settings',
		'menu_slug' 	=> 'bento-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Global Styles',
		'menu_title'	=> 'Global CSS',
		'parent_slug'	=> 'bento-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'bento-general-settings',
	));
		
	
}

 ?>