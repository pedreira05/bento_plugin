<?php 

function register_acf_block_types() {

    // register a testimonial block.
    acf_register_block_type(array(
        'name'              => 'testimonial',
        'title'             => __('Testimonial'),
        'description'       => __('Bento testimonial block.'),
        'render_template'   => BENTO_PLUGIN_PATH . 'blocks/testimonials.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'testimonial', 'quote', 'bento' ),
    ));


    acf_register_block_type(array(
        'name'              => 'banner',
        'title'             => __('Banner'),
        'description'       => __('Bento Banner block.'),
        'render_template'   => BENTO_PLUGIN_PATH . 'blocks/banner.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'banner', 'bento' ),
    ));


    acf_register_block_type(array(
        'name'              => 'featurette_image_right',
        'title'             => __('Featurette - Image Right'),
        'description'       => __('Feature block with image on the right'),
        'render_template'   => BENTO_PLUGIN_PATH . 'blocks/featurette_image_right.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'featurette', 'image', 'text', 'paragraph', 'bento' ),
    ));

    acf_register_block_type(array(
        'name'              => 'featurette_image_left',
        'title'             => __('Featurette - Image Left'),
        'description'       => __('Feature block with image on the left'),
        'render_template'   => BENTO_PLUGIN_PATH . 'blocks/featurette_image_left.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'featurette', 'image', 'text', 'paragraph', 'bento' ),
    ));

    acf_register_block_type(array(
        'name'              => 'marketing',
        'title'             => __('Marketing'),
        'description'       => __('Block of icon and text'),
        'render_template'   => BENTO_PLUGIN_PATH . 'blocks/marketing.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'marketing', 'icon', 'text', 'paragraph', 'bento' ),
    ));

    acf_register_block_type(array(
        'name'              => 'marketing-images',
        'title'             => __('Marketing with Images'),
        'description'       => __('Block of images and text'),
        'render_template'   => BENTO_PLUGIN_PATH . 'blocks/marketing-images.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'marketing', 'image', 'text', 'paragraph', 'bento' ),
    ));

    acf_register_block_type(array(
        'name'              => 'pricing',
        'title'             => __('Pricing'),
        'description'       => __('Add pricing table for products'),
        'render_template'   => BENTO_PLUGIN_PATH . 'blocks/pricing.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'pricing', 'bento' ),
    ));


    acf_register_block_type(array(
        'name'              => 'tab_list',
        'title'             => __('Tab List'),
        'description'       => __('Add Tab list to tab through collapsed content'),
        'render_template'   => BENTO_PLUGIN_PATH . 'blocks/tab_list.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'tab list', 'bento' ),
    ));


    acf_register_block_type(array(
        'name'              => 'album',
        'title'             => __('Album'),
        'description'       => __('Add Album'),
        'render_template'   => BENTO_PLUGIN_PATH . 'blocks/album.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'album', 'images', 'thumbnails', 'portfolio', 'bento' ),
    ));

    acf_register_block_type(array(
        'name'              => 'contact',
        'title'             => __('Contact Us'),
        'description'       => __('Add contact Us block'),
        'render_template'   => BENTO_PLUGIN_PATH . 'blocks/contact.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'contact', 'form', 'sign-up', 'bento' ),
    ));

    acf_register_block_type(array(
        'name'              => 'posts',
        'title'             => __('Posts'),
        'description'       => __('Add Posts block'),
        'render_template'   => BENTO_PLUGIN_PATH . 'blocks/posts.php',
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'posts', 'blog', 'excerpt', 'bento' ),
    ));






}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
}




 ?>