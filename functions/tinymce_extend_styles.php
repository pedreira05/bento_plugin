<?php

function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );


function my_tiny_mce_before_init( $mceInit ) {
	$style_formats = array(
		array(
			'title' => 'PHP',
			'block' => 'code',
			'classes' => 'language-php'
		)	
	);
	$mceInit['style_formats'] = json_encode( $style_formats );	
	return $mceInit;    
}
add_filter( 'tiny_mce_before_init', 'my_tiny_mce_before_init' );


?>